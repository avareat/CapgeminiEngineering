Feature: Two tests for the technical interview for Capgemini Engineering.
  @PruebaTecnicaSearch
   Scenario Outline: PerformSearch

      Given Open Chrome
      Then Navigate To Melia Page "<url>"
      When Introduce Destination "<destination_name>"
      And Introduce Start Day "<start_day>" And Duration Of The Stay "<stay_days>"
      And Introduce Number Of Rooms "<number_rooms>" And People "<people_adults>" And "<people_kids>" With Age "<people_kids_age>"
      #And Introduce Promotional Code "<promotional_code>"
      Then Click Search
      And Verify Search Page "<destination_name>"
      Then Close Chrome

     Examples:
       |url                              | destination_name | start_day | stay_days | number_rooms | people_adults |people_kids|people_kids_age|
       |https://www.melia.com/es/home.htm| BARCELONA        |3/2/2022   |3          |1             |2              |2          |11,12          |

    @PruebaTecnicaCounter
    Scenario Outline: PerformCounter

      Given Open Chrome
      Then Navigate To Melia Page "<url>"
      And Count Number Of Rooms
      And List Room Names
      Then Close Chrome


      Examples:
      |url                                                                                                                          |
      |https://www.melia.com/es/hoteles/espana/madrid/melia-castilla/habitaciones.htm?fechaEntrada=1621015200&fechaSalida=1621188000|