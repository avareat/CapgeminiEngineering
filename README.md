## PREVIEW ##

These tests were performed for the technical part of the interview with Capgemini Engineering.

The tests are written in the path features/PruebaTecnica.feature

The WebDriver is located in root as chromedriver.exe

The tests steps are in the path src/test/java/stepDefinition where you can find a Gherkin translator, a Steps definer and a support class for common methods.

The tests are tagged as @PruebaTecnicaCounter and @PruebaTecnicaSearch.

## EXECUTION ##

We can find a runner in the path src/test/java/runner/TestRunnerNew.java

To execute each test it is mandatory to use its corresponding tag.

The part of the script corresponding to the promotionalcode and its assert has been commented as I could not find a correct code to test that part of the search when using a correct code. The search now defaults to not using a promotional_code.

## IMPROVEMENTS ##

I listed all the rooms found by name in the test @PruebaTecnicaCounter using only one xpath.

I suggest to use TestNG to use a setup and a teardown for the tests.

It also allows to use the newest version of java, since this version of JUnit isn't compatible with the newest Java SE release

## FOUND BUGS ##

When controlling Chrome with an automation tool, the day selector returns an error saying the days haven't been selected, however the days were correctly selected and the search was performed. The execution rate of this bug was 100%

When changing the number of rooms / Adults / Kids sometimes the first click on the increase/decrease button closes the dropdown. The execution rate of this bug is pretty low and i haven't been able to reproduce it consistently.

