package stepDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;

import java.text.ParseException;

public class PruebaTecnicaGherkin extends PruebaTecnicaSteps{
    static WebDriver driver;
    static PruebaTecnicaHelper helper = new PruebaTecnicaHelper();
    static int room_counter;

    @Given("^Open Chrome$")
    public void Open_Chrome() {
        driver = OpenBrowser();
    }
    @Then("^Navigate To Melia Page \"(.*?)\"$")
    public void Navigate_To_Melia_Page(String url) {
        OpenPage(url);
    }
    @When("^Introduce Destination \"(.*?)\"$")
    public void Introduce_Destination(String destination_name) {
        ChooseDestination(destination_name);
    }
    @And("^Introduce Start Day \"(.*?)\" And Duration Of The Stay \"(.*?)\"$")
    public void Introduce_Stay(String start_day, int stay_days) throws ParseException {
        IntroduceStay(start_day, stay_days);
    }
    @And("^Introduce Number Of Rooms \"(.*?)\" And People \"(.*?)\" And \"(.*?)\" With Age \"(.*?)\"$")
    public void Introduce_Rooms_People(int number_rooms, int people_adults, int people_kids, String people_kids_age){
        RoomSelection(number_rooms,people_adults,people_kids,people_kids_age);
    }
    /*@And("^Introduce Promotional Code \"(.*?)\"$")
    public void Introduce_Promotional_Code(String promotional_code){
        if (!promotional_code.equals("null")) {
            PromotionalCode(promotional_code);
        }
    }*/
    @Then("^Click Search$")
    public void Click_Search(){
        helper.Click_Button("mbe-search-button","id" );
    }
    @And("^Verify Search Page \"(.*?)\"$")
    public void Verify_Search_Page(String name){
        VerifySearch(name);
    }
    @Then("^Close Chrome$")
    public void Close_Chrome(){
        driver.quit();
    }
    @And("^Count Number Of Rooms$")
    public void Room_Counter(){
        room_counter = CountElements();
        System.out.println("There are "+room_counter+" rooms");
    }
    @And("^List Room Names$")
    public void List_Room_Names(){
        ListRooms(room_counter);
    }
}