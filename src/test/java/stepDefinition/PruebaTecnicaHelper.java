package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static stepDefinition.PruebaTecnicaSteps.wait;
import static stepDefinition.PruebaTecnicaSteps.driver;

public class PruebaTecnicaHelper {

    private static final Calendar c = Calendar.getInstance();
    private static final SimpleDateFormat date_format = new SimpleDateFormat("yyyy/M/d");
    private static final String destination_name_in_page = "//h3/strong[2]";

    public boolean Check_Presence (String id, String format){
        boolean presence = false;
        switch (format) {
            case "id":
                presence = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id))).isDisplayed();
            break;
            case "xpath":
                presence = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(id))).isDisplayed();
            break;
        }
        return presence;
    }
    public void Click_Button(String id, String format){
        switch (format){
            case "id":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)))
                        .click();
                break;
            case "xpath":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(id)))
                        .click();
                break;
        }
    }
    public void Input_Text (String input_text, String id, String format){
        switch (format){
            case "id":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)))
                        .sendKeys(input_text);
                break;
            case "xpath":
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(id)))
                        .sendKeys(input_text);
                break;
        }
    }
    public String[] Date_Conversor (String start_day, int stay_days) throws ParseException {
        Date date_begin = new SimpleDateFormat("dd/MM/yyyy").parse(start_day);
        String start_date = date_format.format(date_begin);
        c.setTime(date_begin);
        c.add(Calendar.DATE, stay_days);
        Date date_end=c.getTime();
        String end_date = date_format.format(date_end);
        return new String[]{start_date,end_date};
    }
    public String Get_Page_Url(){
        return driver.getCurrentUrl();
    }

    public boolean Check_Name(String name){
        String good_name = name.toLowerCase();
        System.out.println(good_name);
        String actual_name = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(destination_name_in_page))).getText().toLowerCase();
        System.out.println(actual_name);
        return actual_name.contains(good_name);
    }
    public boolean Check_Xpath(String xpath) {
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath))).isDisplayed();
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }
}
