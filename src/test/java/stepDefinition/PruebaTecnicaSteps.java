package stepDefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.text.ParseException;
import java.util.List;

public class PruebaTecnicaSteps extends PruebaTecnicaHelper{

    public static WebDriver driver;
    public static WebDriverWait wait;
    private static final String destination_search_id ="mbe-destination-input";
    private static final String cookies_accept_id ="didomi-notice-agree-button";
    private static final String date_search_id ="mbe-dates-select";
    private static final String add_rooms_xpath="//span[@onclick='too.be.rooms.add()']";
    private static final String remove_adults_xpath="//span[@onclick='too.be.rooms.removeAdult(0)']";
    private static final String add_adults_xpath="//span[@onclick='too.be.rooms.addAdult(0)']";
    private static final String add_kids_xpath="//span[@onclick='too.be.rooms.addChild(0)']";
    private static final String kids_age_input="//input[@class='mbe-input-children-age' and @child='";
    private static final String room_accept_button="//div[@class='mbe-room-btn']/button[@class='mbe-btn']";
    //private static final String promotional_code_id="mbe-coupon-input";
    private static final String room_counter_xpath="//div[contains(@class, 'o-module')]//h4/a";
    //private static final String room_name_xpath="//div[contains(@class, 'o-module')]//h4/a[@data-trigger='room-";
    private static final String no_destination_alert="//div[@class='mbe-input mbe-input-destination tooltipstered mbe-alert']";
    //private static final String coupon_not_valid_alert="//div[@class='mbe-input mbe-input-coupon searchScroll']/span[@class='ico-error']";
    private static final String destination_page_url="https://booking.melia.com/es/new/buscar/hoteles-disponibles.htm";

    public WebDriver OpenBrowser(){
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver=new ChromeDriver();
        driver.manage().window().maximize();
        wait =  new WebDriverWait(driver, 10);
        return driver;
    }
    public void OpenPage(String url){
        driver.get(url);
        if (Check_Presence(cookies_accept_id, "id")) {
            Click_Button(cookies_accept_id, "id");
        }
    }
    public void ChooseDestination(String destination_name){
        Click_Button(destination_search_id, "id" );
        Input_Text(destination_name, destination_search_id, "id");
    }
    public void IntroduceStay(String start_day, int stay_days) throws ParseException {
        Click_Button(date_search_id,"id" );
        String[] date_array=Date_Conversor(start_day, stay_days);
        String start_date = date_array[0];
        String end_date = date_array[1];
        Click_Button("//li[@d='"+start_date+"']/a", "xpath");
        Click_Button("//li[@d='"+end_date+"']/a", "xpath");
        Assert.assertFalse("The current location is not valid",
                Check_Xpath(no_destination_alert));
    }
    public void RoomSelection(int number_rooms, int people_adults, int people_kids, String people_kids_age){
        int room_difference = number_rooms-1;
        int adults_difference = people_adults-2;
        for (int i=0;i<room_difference;++i){
            Click_Button(add_rooms_xpath, "xpath");
        }
        if (adults_difference == -1) {
            Click_Button(remove_adults_xpath, "xpath");
        }
        else{
            for (int i = 0; i < adults_difference; ++i) {
                Click_Button(add_adults_xpath, "xpath");
            }
        }
        for (int i=0;i<people_kids;++i) {
            Click_Button(add_kids_xpath, "xpath");
        }
        if (people_kids > 0) {
            String [] kids_age = people_kids_age.split(",");
            for (int i = 0; i < people_kids;++i){
                Click_Button(kids_age_input+i+"']", "xpath");
                Input_Text(kids_age[i],kids_age_input+i+"']", "xpath");
            }
        }
        Click_Button(room_accept_button, "xpath");
    }
    /*public void PromotionalCode(String promotional_code){
        Input_Text(promotional_code,promotional_code_id, "id");
    }*/
    public int CountElements(){
        List<WebElement> rooms = driver.findElements(By.xpath(room_counter_xpath));
        return  rooms.size();
    }
    public void ListRooms(int room_counter){
        for (int i =1;i<=room_counter;++i){
            System.out.println("Room "+i+": "+driver.findElements(By.xpath(room_counter_xpath)).get(i-1).getText());
            //System.out.println("Room "+i+": "+Return_Value(room_name_xpath+i+"']"));
        }
    }
    public void VerifySearch(String name){
        //Assert.assertFalse("The current coupon is not valid",Check_Xpath(coupon_not_valid_alert));
        Assert.assertEquals("Expected page is not displayed",
                destination_page_url,
                Get_Page_Url() );
        Assert.assertTrue("Destination is not correctly displayed in page",
                Check_Name(name));

    }
}